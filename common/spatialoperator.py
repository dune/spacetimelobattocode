# interface operator for Runge-Kutta solvers (right-hand-side)
class SpatialOperator:
    def __init__(self, L_h, space, time, dt, cfl=0.25):
        from dune.fem.operator import molGalerkin as molGalerkin
        # create method of lines Galerkin operator L from PDE form
        self._op = molGalerkin( L_h )
        self.cfl = cfl
        self.space = space
        self.time  = time
        self.dt = dt
        # discrete functions uTmp and vTmp
        self.uTmp = space.interpolate([0]*space.dimRange, name='uTmp')
        self.vTmp = space.interpolate([0]*space.dimRange, name='vTmp')
        self.localTimeStepEstimate = [self.dt.value/self.cfl]
        self._jac = None

    # v = L[u]
    def apply(self, u, v):
        self._op(u,v)
        self.localTimeStepEstimate = [self.dt.value/self.cfl]
        return

    # jacobian of L[\bar{u}]
    def jacobian(self, ubar):
        from dune.fem.operator import linear as linearOperator
        if self._jac is None:
            # create Jacobian
            self._jac = linearOperator(self._op, ubar=ubar)
        else:
            # update Jacobian
            self._op.jacobian(ubar, self._jac )
        return self._jac.as_numpy

    # v = L[u]
    def __call__(self,u,v):
        self.apply(u,v)
        return

    # make current simulation time known to operator
    def setTime(self, t):
        self.time.value = t
        self._t  = t

    def stepTime(self,t0, dt0):
        if hasattr(self._op.model, "time"):
            print(f"Setting time to {self._t} + {t0 * dt}")
            self._op.model.time.value = self._t + t0 * dt.value
        else:
            self.time.value = self._t + t0 * dt.value

    def applyLimiter(self, u):
        pass

    ####################################################
    # rhs function for Assimulo forwarding to F
    ####################################################
    def rhs(self, t, y):
        """ Function that calculates the right-hand-side. Depending on
            the problem and the support of the solver, this function has
            the following input parameters:

            rhs(t,y)      - Normal ODE
        """
        ## set time for PDE operator
        self.setTime(t)
        # copy content of y into uTmp
        self.uTmp.as_numpy[:] = y[:]
        # apply spatial discretization operator L
        self.apply(self.uTmp, self.vTmp)
        # store result in y
        yn = y.copy()
        yn[:] = self.vTmp.as_numpy[:]
        return yn

    # return DF(t,y)
    def jac(self, t, y, sw=None):
        # copy content of y into uTmp
        self.uTmp.as_numpy[:] = y[:]
        return self.jacobian( self.uTmp )
