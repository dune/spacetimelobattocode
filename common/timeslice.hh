#ifndef DUNE_FEM_TIMESLICE_HH
#define DUNE_FEM_TIMESLICE_HH

#include <dune/common/exceptions.hh>
#include <dune/fem/common/coordinate.hh>
#include <dune/fem/quadrature/cachingquadrature.hh>
#include <dune/grid/utility/hierarchicsearch.hh>
#include <dune/fem/function/localfunction/const.hh>
#include <dune/grid/utility/hierarchicsearch.hh>
#include <dune/fem/function/common/localfunctionadapter.hh>
#include <dune/fem/function/common/gridfunctionadapter.hh>
#include <dune/fem/space/common/interpolate.hh>

template <class DiscreteFunction>
struct SlicedFunction
{
  typedef DiscreteFunction DiscreteFunctionType;
  typedef typename DiscreteFunctionType :: DiscreteFunctionSpaceType  DiscreteFunctionSpaceType;
  typedef typename DiscreteFunctionSpaceType :: FunctionSpaceType  FunctionSpaceType;
  typedef typename DiscreteFunctionSpaceType :: GridPartType   GridPartType ;
  typedef typename GridPartType :: GridType   GridType;
  typedef typename GridPartType :: IndexSetType IndexSetType;

  typedef typename GridType :: template Codim<0> :: Entity     EntityType;

  typedef typename DiscreteFunctionSpaceType::DomainType DomainType;
  typedef typename DiscreteFunctionSpaceType::RangeType  RangeType;
  typedef typename DiscreteFunctionSpaceType::JacobianRangeType  JacobianRangeType;

  typedef Dune::HierarchicSearch< GridType, IndexSetType > HierarchicSearchType;
  typedef Dune::Fem::ConstLocalFunction< DiscreteFunctionType > ConstLocalFunctionType;

  SlicedFunction( const DiscreteFunction& uh, const double val, const int comp )
    : uh_( uh ), uLocal_( uh_ ),
      hierarchicSearch_( uh_.space().gridPart().grid(), uh_.space().gridPart().indexSet() ),
      val_( val ), comp_( comp )
  {}

  template <class Domain>
  void evaluate( const Domain& xGlobal, RangeType& res ) const
  {
    DomainType X( val_ );
    for( int d=0, r=0 ; d<Domain::dimension; ++d, ++r )
    {
      // skip comp since X was initialized with val
      if( d == comp_ )
      {
        ++r;
      }
      X[ r ] = xGlobal[ d ];
    }
    //std::cout << "Eval on X = " << X << std::endl;

    EntityType entity = hierarchicSearch_.findEntity( X );
    uLocal_.bind( entity );

    const auto local = entity.geometry().local( X );
    uLocal_.evaluate( local, res );
  }

  template <class Domain>
  void jacobian( const Domain& xGlobal, JacobianRangeType& jac ) const
  {
    std::abort();
  }

  const DiscreteFunctionType& uh_;
  mutable ConstLocalFunctionType uLocal_;
  HierarchicSearchType hierarchicSearch_;

  const double val_;
  const int comp_;
};

template <class DiscreteFunction>
struct FixedTimeLocalFunction
{
  typedef DiscreteFunction DiscreteFunctionType;
  typedef typename DiscreteFunctionType :: DiscreteFunctionSpaceType  DiscreteFunctionSpaceType;
  typedef typename DiscreteFunctionSpaceType :: FunctionSpaceType  FunctionSpaceType;
  typedef typename DiscreteFunctionSpaceType :: GridPartType   GridPartType ;
  typedef typename GridPartType :: GridType   GridType;
  typedef typename GridPartType :: IndexSetType IndexSetType;

  typedef typename GridType :: template Codim<0> :: Entity     EntityType;

  typedef typename DiscreteFunctionSpaceType::DomainType DomainType;
  typedef typename DiscreteFunctionSpaceType::RangeType  RangeType;
  typedef typename DiscreteFunctionSpaceType::JacobianRangeType  JacobianRangeType;
  typedef typename DiscreteFunctionSpaceType::HessianRangeType   HessianRangeType;

  typedef Dune::Fem::ConstLocalFunction< DiscreteFunctionType > ConstLocalFunctionType;

  FixedTimeLocalFunction( const DiscreteFunction& uh, const double val, const int comp )
    : uh_( uh ), uLocal_( uh_ ),
      val_( val ), comp_( comp )
  {}

  void init( const EntityType& entity )
  {
    uLocal_.bind( entity );
  }

  template <class Point>
  void evaluate( const Point& xLocal, RangeType& res ) const
  {
    const Dune::FieldVector<double,DiscreteFunctionSpaceType::dimDomain>&
      x = Dune::Fem::coordinate( xLocal );
    DomainType xLoc( x );
    xLoc[ comp_ ] = val_; // endtime
    uLocal_.evaluate( xLoc, res );
  }

  template <class Domain>
  void jacobian( const Domain& xGlobal, JacobianRangeType& jac ) const
  {
    std::abort();
  }

  template <class Domain>
  void hessian( const Domain& xGlobal, HessianRangeType& jac ) const
  {
    std::abort();
  }

  const DiscreteFunctionType& uh_;
  mutable ConstLocalFunctionType uLocal_;

  const double val_;
  const int comp_;
};

template <class DiscreteFunction, class SliceDiscreteFunction >
void timeslice( const DiscreteFunction& uh, SliceDiscreteFunction& slicedU,
                const double val, // value to be fixed (i.e. time)
                const int comp ) // position in coordinate vector
{
  static_assert( int(DiscreteFunction::DiscreteFunctionSpaceType::dimRange) ==
                 int(SliceDiscreteFunction::DiscreteFunctionSpaceType::dimRange),
                 "timeslice: dimRange of discrete functions need to match!");

  typedef SlicedFunction< DiscreteFunction > SlicedFunctionType;
  SlicedFunctionType slice( uh, val, comp );

  auto slicedGF = Dune::Fem::gridFunctionAdapter( slice, slicedU.space().gridPart(), uh.space().order() );

  Dune::Fem::interpolate( slicedGF, slicedU );
}

template <class DiscreteFunction >
void shiftToEndTime( const DiscreteFunction& arg, DiscreteFunction& dest,
                     const int comp ) // position in coordinate vector
{
  typedef typename Dune::Fem::LocalFunctionAdapter< FixedTimeLocalFunction< DiscreteFunction > > LocalFunctionAdapterType;
  FixedTimeLocalFunction< DiscreteFunction > lf( arg, 1.0, comp ); // 1.0 is endTime in local coordinates

  LocalFunctionAdapterType fixedTime( "fixedTime", lf, arg.space().gridPart(), arg.space().order() );

  Dune::Fem::interpolate( fixedTime, dest );
}
#endif
