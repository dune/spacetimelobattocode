#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from ufl import as_vector, conditional
from ufl import pi, dot, cos, sin

domainLower = 0.
domainUpper = 1.

# computational domain
def domain( dim ):
    return [0.] * dim, [1.] * dim

# space time exact solution
def exact(x, t):
    """ Smooth bubble advection problem for Euler """
    dim = len(x)

    gamma = 1.4

    v = [ sin(pi/5.0) ]*dim
    v[ 0 ] = cos(pi/5.0)

    p = 0.3

    tmp=0.0
    for i in range(dim):
        tmp_c = x[i] - 0.25 - t * v[i]
        tmp += tmp_c*tmp_c

    tmp *= 16.0;

    cos_tmp = cos(tmp * pi) + 1.0
    rho = conditional( tmp > 1.0, 0.5, 0.25 * cos_tmp*cos_tmp + 0.5)

    v = as_vector(v)
    rE = p/(gamma-1) + 0.5*rho*dot(v,v)

    return as_vector([rho, *[rho*u for u in v], rE])

# space time exact solution
def spacetimeexact(x,t):
    x_s = as_vector([x[i] for i in range(len(x)-1)])
    return exact(x_s,t)
