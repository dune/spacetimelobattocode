#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from ufl import as_vector, conditional
from ufl import exp, pi, dot, cos, sin

# computational domain
def domain( dim ):
    return [-10.] * dim, [10.] * dim

# space time exact solution
def exact(x, t):
    dim = len(x)
    assert dim >= 2, "Isentropic Vortex for 2d and 3d"

    S = 5.0     # Strength
    M = 0.5     # Mach number
    gamma = 1.4

    x0 = x[0] - t + 1.
    x1 = x[1]

    f = 1 - x0*x0 - x1*x1

    #if dim > 2:
    #    x2 = x[2]
    #    f -= x2*x2

    rho = (1 - S*S*(gamma-1)*M*M*exp(f)/(8*pi*pi))**(1/(gamma-1))
    v = [0]*dim
    # v_x
    v[0] = 1 - S*x1*exp(f/2)/(2*pi)
    # v_y
    v[1] = S*x0*exp(f/2)/(2*pi)
    p = pow(rho, gamma)/(gamma*M*M)

    #if dim > 2:
    #    # v_z
    #    v[2] = 0. # S*x2*exp(f/2)/(2*pi)
    v = as_vector(v)

    rE = p/(gamma-1) + 0.5*rho*dot(v,v)

    return as_vector([rho, *[rho*u for u in v], rE])

# space time exact solution
def spacetimeexact(x,t):
    x_s = as_vector([x[i] for i in range(len(x)-1)])
    return exact(x_s,t)
